#include <SDL2/SDL.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define WIDTH 512
#define HEIGHT 256
#define PIXEL_SIZE 8
#define COLUM_WIDTH 64
#define ROW_HEIGHT 32
#define num_pixels 8192
#define pos_max 32 

// Definición de colores
#define color_white    0x00ffffff
#define color_pink     0x00FF00FF
#define color_black    0x00000000
#define color_red      0x00ff0000
#define color_cyan     0x0000ffff
#define color_orange   0x00ffa500
#define color_gray     0x00808080
#define color_blue     0x000000ff
#define color_green    0x0000ff00
#define color_yellow   0x00FFFF00
#define color_purple   0x800080
#define color_brown    0xc19a6b00
#define color_lblue    0x0000CCFF
#define color_rosybrown 0x00bc8f8f
#define color_burdeos  0x800020

  char last_7[8];
  char last[8]; 
  char interval_funct3[4];
  char interval_funct7[8];
  char interval_IMM[13];
  char interval_rd[5];
  char interval_rs1[5];
  char interval_rs2[5];
  char interval_imm_I[13];
  char interval_imm_U[20];  
  char interval_imm_S1[7];
  char interval_imm_S2[6];
	 char interval_imm_SB1[6];
		char interval_imm_SB2[7]; 
		     
		char interval_Imm_SB1[7];
		char interval_Imm_SB2[5];
		char interval_imm_Sb1[6];
		char interval_imm_Sb2[4];
		char interval_Immediate[12]; 
		  
		char interval_Imm_UJ[20];
		char interval_Imm_UJ1[11];
		char interval_Imm_UJ2[9];
		char interval_Immediat[21]; 
		char prueba[1];
	 char prove[8];
	 char csr[7];
		char char_save[100];
 //Estructura de registros   
typedef struct{
    char pos[6]; 
    char value[10]; 
    int  save[256]; 
    
} Dictionary;
//Estructura de coordenadas
struct coorde {
    int pos_x;
    int pos_y;
    int color;
};

    SDL_Window *window = NULL;
    SDL_Renderer *renderer = NULL;
    
struct coorde pixels[num_pixels];

    // Lista de opcodes
    char *lista[] = {"0000011", "0001111", "0010011", "0010111", "0011011", "0100011", "0110011","0110111","0111011", "1100011", "1100111", "1101111","1110011"}; 
    			//I	     //I      //I	  //U        //I        //S        //R       //U       //R      //SB       // I        //UJ       //I
    // Lista de funct3
    char *funct3[] = {"000", "001", "010", "011", "100", "101", "110", "111"};

    // Lista de funct7
    char *funct7[] = {"0000000", "0100000","0000001"}; 
        
    // Lista de IMM
    char *IMM[] = {"000000000000", "000000000001"};
    
    int inst_sp[34];

				int arreglo[1200];
    
    Dictionary regist[pos_max] = {
        {"00000", "zero",0},{"00001", "ra",0},{"00010", "sp",0},{"00011", "gp",0},{"00100", "tp",0},{"00101", "t0",0},{"00110", "t1",0},{"00111", "t2",0},{"01000", "s0",0},{"01001", "s1",0},{"01010", "a0",0},{"01011", "a1",0},{"01100", "a2",0},{"01101", "a3",0},{"01110", "a4",0},{"01111", "a5",0},{"10000", "a6",0},{"10001", "a7",0},{"10010", "s2",0},{"10011", "s3",0},{"10100", "s4",0},{"10101", "s5",0},{"10110", "s6",0},{"10111", "s7",0},{"11000", "s8",0},{"11001", "s9",0},{"11010", "s10",0},{"11011", "s11",0},{"11100", "t3",0},{"11101", "t4",0},{"11110", "t5",0},{"11111", "t6",0}
}; 

int imm_auipc = 0;
int imm_lui = 0;
int sp = 0; // stack pointer
int key = 0;
int Esc = 0;

// Función para convertir una cadena binaria en complemento a dos a un entero con signo
/*int binario_a_entero(const char *binario) {
    int longitud = strlen(binario);
    int resultado = 0;
    int signo = (binario[0] == '1') ? -1 : 1; 

    for (int i = longitud - 1; i >= 0; i--) {
        if (binario[i] == '1') {
            resultado += pow(2, longitud - i - 1);
        }
    }
    
    // Aplicar el signo
    resultado *= signo;
    
    return resultado;
}*/



int initSDL(SDL_Window **window, SDL_Renderer **renderer) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    *window = SDL_CreateWindow("Emulador RISC-V", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, COLUM_WIDTH * PIXEL_SIZE, ROW_HEIGHT * PIXEL_SIZE, 0);
    if (*window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
    if (*renderer == NULL) {
        printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
        SDL_DestroyWindow(*window);
        SDL_Quit();
        return 1;
    }
        if (SDL_Init(SDL_INIT_AUDIO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return 1;
    }

    return 0;
}

void SDL_Delay(Uint32 ms);
void Draw(SDL_Renderer *renderer) {
    int pos;
    
    for (int r = 0; r < num_pixels; r++) {
        SDL_Rect pixelRect = {pixels[r].pos_x * PIXEL_SIZE, pixels[r].pos_y * PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE};
        pos = pixels[r].color;
        switch(pos) {
            case color_white: SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); break;
            case color_red: SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255); break;
            case color_black: SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); break;
            case color_cyan: SDL_SetRenderDrawColor(renderer, 0, 255, 255, 255); break;
            case color_orange: SDL_SetRenderDrawColor(renderer, 255, 165, 0, 255); break;
            case color_green: SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255); break;
            case color_blue: SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255); break;
            case color_purple: SDL_SetRenderDrawColor(renderer, 128, 0, 128, 255); break;
            case color_brown: SDL_SetRenderDrawColor(renderer, 165, 42, 42, 255); break;
            case color_yellow: SDL_SetRenderDrawColor(renderer, 255, 255, 0, 255); break;
            case color_pink: SDL_SetRenderDrawColor(renderer, 255, 0, 255, 255); break;
            case color_gray: SDL_SetRenderDrawColor(renderer, 128, 128, 128, 255); break;
            case color_lblue: SDL_SetRenderDrawColor(renderer, 0, 204, 255, 255); break;
            case color_rosybrown: SDL_SetRenderDrawColor(renderer, 188, 143, 143, 255); break;
            case color_burdeos: SDL_SetRenderDrawColor(renderer, 128, 0, 32, 255); break;
            default: SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); break;
        }

        SDL_RenderFillRect(renderer, &pixelRect);
    }
      SDL_Rect pixelRect = {0 * PIXEL_SIZE,0 * PIXEL_SIZE, PIXEL_SIZE, PIXEL_SIZE};
      int h = pixels[1].color;
      switch(h){
            case color_gray: SDL_SetRenderDrawColor(renderer, 128, 128, 128, 255); break;
      } 
      SDL_RenderFillRect(renderer, &pixelRect);
}

int main(int argc, char *args[]) {

if (initSDL(&window, &renderer) != 0) {
        return 1;
    }
   
    FILE *archivo;
    FILE *archivo_2;

    char **lineas = NULL; 
    char **lines_Data = NULL;
    char linea[1000];
    char lines[1000];
 
    int num_lineas = 0;
    int num_lines_data = 0; 
    srand(time(NULL));
    struct timespec ts;
    
    // Abrir el archivo
    archivo = fopen("TEXT BINARY.text", "r");

    if (archivo == NULL) {
        printf("No se pudo abrir el archivo.\n");
        return 1;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    while (fgets(linea, sizeof(linea), archivo)) {
    
        lineas = realloc(lineas, (num_lineas + 1) * sizeof(char *));

        lineas[num_lineas] = malloc(strlen(linea) + 1);
        
        strcpy(lineas[num_lineas], linea);
        num_lineas++;
    }

    fclose(archivo);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	  archivo_2 = fopen("DATA TEXT", "r");
	  if (archivo_2 == NULL) {
	      printf("No se pudo abrir el archivo.\n");
	      return 1;
	  }

    while (fgets(lines, sizeof(lines), archivo_2)) {
    
        lines_Data = realloc(lines_Data, (num_lines_data + 1) * sizeof(char *));

        lines_Data[num_lines_data] = malloc(strlen(lines) + 1);
        
        strcpy(lines_Data[num_lines_data], lines);
        num_lines_data++;
    }

    fclose(archivo_2);

///////////////////////////////////////////////////////////////////////////////////////////////////////
    int quit = 0;
    SDL_Event e;

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
        
///////////////////////////////////////////////////////////////////////////////////////////////////////////
				for (int z = 0; z < num_lines_data; z++){								
									int var =	strtol(lines_Data[z], NULL, 2);
									if(lines_Data[z][0] == '1'){
												var -= (1L << 32);
									}	
										arreglo[z] = var;
				}

    int i = 0;
    while ( i < num_lineas && Esc == 0) {
    	int lin = i+1;
    	
    	int address;
    	address = 0x00400000 + i*4;

					while (SDL_PollEvent(&e) != 0) {
								        if (e.type == SDL_QUIT) {
								            quit = 1;
								        }
								    }
								    		    
						SDL_PumpEvents(); // Actualizar el estado del teclado
						const Uint8 *keystate = SDL_GetKeyboardState(NULL);				
						if (keystate[SDL_SCANCODE_1]) {key = 49;}
						if (keystate[SDL_SCANCODE_2]) {key = 50;}
	  	  if (keystate[SDL_SCANCODE_W]) {key = 119;}   
	  			if (keystate[SDL_SCANCODE_S]) {key = 115;}
	  	  if (keystate[SDL_SCANCODE_D]) {key = 100;}   
	  			if (keystate[SDL_SCANCODE_A]) {key = 97;}	  
	  			if (keystate[SDL_SCANCODE_B]) {key = 98;}	 
	  			if (keystate[SDL_SCANCODE_O]) {key = 111;}	  
	  			if (keystate[SDL_SCANCODE_L]) {key = 108;}		  			 			  						
	   		if (keystate[SDL_SCANCODE_SPACE]) {key = 32;}
	  			if (keystate[SDL_SCANCODE_ESCAPE]) {Esc = 1;}	 
	  			//////////////////////////////////////////////////////////////////////////////////////////////////////////
	  			//INSTRUCCIONES TIPO I	  			  		
        strncpy(last_7, lineas[i] + 25, 7); 
        last_7[7] = '\0';
								//Ciclo de opcodes
        for (int k = 0; k < sizeof(lista) / sizeof(lista[0]); k++) {
            if (strcmp(last_7, lista[k]) == 0) {
                if (k == 0) {
                    
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if (h == 0){  	    
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {																				
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {																
																					// Inmediato
																					strncpy(interval_imm_I, lineas[i] + 0, 12); 
																					interval_imm_I[12] = '\0';

                     long decimal = strtol(interval_imm_I, NULL, 2);
                    
                     if (interval_imm_I[0] == '1') {
                         decimal -= (1L << 12);
                     }
																					printf("%d I, lb %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																		      break;
																			}
																		}
																	}									    	 	 
						    	  						}                    	    	  
                    	    }else if (h == 1){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {																			
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						// Inmediato
																						strncpy(interval_imm_I, lineas[i] + 0, 12); 
																						interval_imm_I[12] = '\0';
																						
                      long decimal = strtol(interval_imm_I, NULL, 2);
                     
                      if (interval_imm_I[0] == '1') {
                          decimal -= (1L << 12);
                      }
																						printf("%d I, lh %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																			      break;
																			}
																		}
																	}									    	 	 
						    	 							}                    	          
                    	    }else if (h == 2){  // INSTRUCCION lw                 	    
                   // Registro rd
                    strncpy(interval_rd, lineas[i] + 20, 5);
                    interval_rd[5] = '\0';

                    for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
                        if (strcmp(interval_rd, regist[l].pos) == 0) {
                            // Registro rs1
                            strncpy(interval_rs1, lineas[i] + 12, 5);
                            interval_rs1[5] = '\0';

                            for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
                                if (strcmp(interval_rs1, regist[t].pos) == 0) {
                                    int x = 0x10010000;
                                    regist[0].save[0] = 0;

                                    // Inmediato
                                    strncpy(interval_imm_I, lineas[i] + 0, 12);
                                    interval_imm_I[12] = '\0';

                                    long decimal = strtol(interval_imm_I, NULL, 2);
                                    if (interval_imm_I[0] == '1') {
                                        decimal -= (1L << 12);
                                    }
																																					//stack
                                    if (strcmp(regist[t].value, "sp") == 0) {
                                        regist[l].save[0] = inst_sp[(sp + decimal) / 4];
                                        printf("%d I, address:%x, D.regis:%d lw %s, %ld(sp)\n", lin, address, regist[l].save[0], regist[l].value, decimal);
                                    } else {
                                        long pote = (long)pow(2, 12);
                                        long potencia = imm_auipc * pote;
                                        //Posicion en el data
                                        int resultado = ((address - 4) + potencia + decimal - x) / 4;

																																								strncpy(prove, lineas[i-1] + 25, 7); 
																																								prove[7] = '\0';
																																								
																																								if (strcmp(lista[3], prove) == 0) {                                      				
                                            regist[l].save[0] = arreglo[resultado];
																																																								
                                            printf("%d I, address:%x Pos_Data:%d, D.regis t0= %d lw %s, %ld(%s)\n", lin, address, resultado, regist[l].save[0], regist[l].value, decimal, regist[t].value);
                                        } else {
                                            printf("%d I, address:%x Pos_Data:%d, D.regis:%d lw %s, %ld(%s)\n", lin, address, resultado, regist[l].save[0], regist[l].value, decimal, regist[t].value);
                                        }
                                           //Asiganacion de teclas
																																       						strncpy(last, lineas[i-1] + 25, 7); 
																																													last[7] = '\0';
																																									if (strcmp(lista[7], last) == 0 && imm_lui == -16){
																																											regist[l].save[0] = key;																																											
																																									}
																																									//Colission
																																									if (strcmp(regist[l].value,"a2") == 0 && strcmp(regist[t].value,"t1") == 0){
																																													int v = regist[11].save[0]*64 + regist[10].save[0];
																																													regist[l].save[0] = pixels[v].color;
																																									} 
                                    }
                                }
                            }
                        }
                    }
                }else if (h == 3){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0';

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									regist[0].save[0] = 0;
																									// Inmediato
																									strncpy(interval_imm_I, lineas[i] + 0, 12); 
																									interval_imm_I[12] = '\0';
																									
                                                  long decimal = strtol(interval_imm_I, NULL, 2);
                                                 
		                                              if (interval_imm_I[0] == '1') {
		                                                  decimal -= (1L << 12);
		                                              }
																									printf("%d I, ld %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																						      break;
																				}
																		}
																}									    	 	 
									    	 	 }                    	          
                    	    }else if (h == 4){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 
																		
																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						// Inmediato
																						strncpy(interval_imm_I, lineas[i] + 0, 12); 
																						interval_imm_I[12] = '\0';
																						
                                            long decimal = strtol(interval_imm_I, NULL, 2);
                                           
                                            if (interval_imm_I[0] == '1') {
                                                decimal -= (1L << 12);
                                            }
																						printf("%d I, lbu %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																			      break;
																			}
																		}
																	}									    	 	 
						    	 							}                   	          
                    	    }else if (h == 5){
																		//Registro rd						
																		strncpy(interval_rd, lineas[i] + 20, 5); 
																		interval_rd[5] = '\0'; 
																		
																		for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																			if (strcmp(interval_rd, regist[l].pos) == 0) {
																			
																				// Registro rs1
																				strncpy(interval_rs1, lineas[i] + 12, 5); 
																				interval_rs1[5] = '\0'; 
																				for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																					if (strcmp(interval_rs1, regist[t].pos) == 0) {

																								// Inmediato
																								strncpy(interval_imm_I, lineas[i] + 0, 12); 
																								interval_imm_I[12] = '\0';
																								
                        long decimal = strtol(interval_imm_I, NULL, 2);
                        
                        if (interval_imm_I[0] == '1') {
                            decimal -= (1L << 12);
                        }
																								printf("%d I, lhu %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																					      break;
																					}
																			}
																	}									    	 	 
								    	 				}                    	          
                    	    }else if (h == 6){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 
																			
																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									// Inmediato
																									strncpy(interval_imm_I, lineas[i] + 0, 12); 
																									interval_imm_I[12] = '\0';
																									
                                                  long decimal = strtol(interval_imm_I, NULL, 2);
                                                 
		                                              if (interval_imm_I[0] == '1') {
		                                                  decimal -= (1L << 12);
		                                              }
																									printf("%d I, lwu %s, %ld(%s)\n",lin, regist[l].value, decimal, regist[t].value);																									
																						      break;
																						}
																					}
																				}									    	 	 
									    	   						}                    	          
                    	   					 }
                    						}             	            
                   					 }
         
                } else if (k == 1){
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){

																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, fence %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																				}
																		}
																}									    	 	 
									    	 	}                    	    	  
                    	    }else if (h == 1){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 
																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, fence.i %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																						}
																					}
																				}									    	 	 
									    	 							}                    	          
                    	  					 }
                    						}
                    					}
                }else if (k == 2){
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																					if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																							if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						      regist[0].save[0] = 0;																									
																									// Inmediato
																									strncpy(interval_imm_I, lineas[i] + 0, 12); 
																									interval_imm_I[12] = '\0';
																									
                               long decimal = strtol(interval_imm_I, NULL, 2);
                               
                             if (interval_imm_I[0] == '1') {
                                 decimal -= (1L << 12);
                             }
		                                              	                                              
		                           strncpy(last, lineas[i-1] + 25, 7); 
        																					last[7] = '\0';

																									if (strcmp(lista[7], last) == 0){

																											decimal = imm_lui * pow(2, 12) ;
																											regist[t].save[0] = 0;
																											
																									}	
																									if (strcmp(regist[l].value, "sp") == 0) {
                        								sp += decimal;
                        								printf("Valor contador: %d\n", sp);
                   							  }																								
																								  regist[l].save[0] = regist[t].save[0] + decimal;                                        
																									printf("%d I, address:%x addi %s = %d, %s = %d, %ld\n",lin,address, regist[l].value, regist[l].save[0], regist[t].value,regist[t].save[0], decimal);
																									break;																								
																				}
																		}
																}									    	 	 
									    	  	}                   	    	  
                    	    }else if (h == 1){
																// comparar funct7
																strncpy(interval_funct7, lineas[i] + 0, 7); 
																for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																	if (strcmp(interval_funct7, funct7[p]) == 0){
																			if ( p == 0){

																					//Registro rd						
																					strncpy(interval_rd, lineas[i] + 20, 5); 
																					interval_rd[5] = '\0'; 

																					for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																						if (strcmp(interval_rd, regist[l].pos) == 0) {
																						
																							// Registro rs1
																							strncpy(interval_rs1, lineas[i] + 12, 5); 
																							interval_rs1[5] = '\0'; 

																							for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																								if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									// Inmediato
																									strncpy(interval_imm_I, lineas[i] + 0, 12); 
																									interval_imm_I[12] = '\0';
														
																									long decimal = strtol(interval_imm_I, NULL, 2);																								
																									if (interval_imm_I[0] == '1') {
																											decimal -= (1L << 12);
																									}
																									printf("%d I, slli %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);																											
																									break;
																						    }
																							}
																						}									    	 	 
														 							}																					
																				}
																			}
																	 }
                    	    }else if (h == 2){
																		//Registro rd						
																		strncpy(interval_rd, lineas[i] + 20, 5); 
																		interval_rd[5] = '\0'; 

																		for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																			if (strcmp(interval_rd, regist[l].pos) == 0) {
																			
																				// Registro rs1
																				strncpy(interval_rs1, lineas[i] + 12, 5); 
																				interval_rs1[5] = '\0'; 

																				for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																					if (strcmp(interval_rs1, regist[t].pos) == 0) {
																								// Inmediato
																								strncpy(interval_imm_I, lineas[i] + 0, 12); 
																								interval_imm_I[12] = '\0';
																								
                                                long decimal = strtol(interval_imm_I, NULL, 2);
                                                
	                                              if (interval_imm_I[0] == '1') {
	                                                  decimal -= (1L << 12);
	                                              }
																								printf("%d I, slti %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																								break;
																			   }
																			}
																	}									    	 	 
								    	 				}                    	          
                    	    }else if (h == 3){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						// Inmediato
																						strncpy(interval_imm_I, lineas[i] + 0, 12); 
																						interval_imm_I[12] = '\0';																								

                                            long decimal = strtol(interval_imm_I, NULL, 2);
                                            
                                            if (interval_imm_I[0] == '1') {
                                                decimal -= (1L << 12);
                                            }
																						printf("%d I, sltiu %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																						break;
																	   }
															    }
													     }									    	 	 
						    	        	}                    	          
                    	    }else if (h == 4){
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 
																			
																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {
																								regist[0].save[0] = 0;
																							// Inmediato
																							strncpy(interval_imm_I, lineas[i] + 0, 12); 
																							interval_imm_I[12] = '\0';																								

                       long decimal = strtol(interval_imm_I, NULL, 2);
                       
                       if (interval_imm_I[0] == '1') {
                           decimal -= (1L << 12);
                       }
                        regist[l].save[0] = regist[t].save[0] ^ decimal;
                                              
																							printf("%d I, xori %s = %d, %s = %d, %ld\n",lin, regist[l].value,regist[l].save[0], regist[t].value,regist[t].save[0], decimal);
																							break;
																				}
																			}
																		}									    	 	 
							    	 							}                    	          
                    	    }else if (h == 5){                 	          
															 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){

																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 
																					
																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {

																									// Inmediato
																									strncpy(interval_imm_I, lineas[i] + 0, 12); 
																									interval_imm_I[12] = '\0';
																									
                                                  long decimal = strtol(interval_imm_I, NULL, 2);
                                                  
		                                              if (interval_imm_I[0] == '1') {
		                                                  decimal -= (1L << 12);
		                                              }
																									printf("%d I, srli %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																									break;
																				}
																		}
																}									    	 	 
									    	 	}																		 	 
																	}else if( p == 1){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, srai %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																						}
																					}
																				}									    	 	 
									    	 							}																			 
																		}
																	}
															 }
                    	    }else if (h == 6){
															//Registro rd						
															strncpy(interval_rd, lineas[i] + 20, 5); 
															interval_rd[5] = '\0'; 
															
															for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																if (strcmp(interval_rd, regist[l].pos) == 0) {
																
																	// Registro rs1
																	strncpy(interval_rs1, lineas[i] + 12, 5); 
																	interval_rs1[5] = '\0'; 
																	
																	for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																		if (strcmp(interval_rs1, regist[t].pos) == 0) {

																					// Inmediato
																					strncpy(interval_imm_I, lineas[i] + 0, 12); 
																					interval_imm_I[12] = '\0';																							

															                  long decimal = strtol(interval_imm_I, NULL, 2);
															                  
															                  if (interval_imm_I[0] == '1') {
															                      decimal -= (1L << 12);
															                  }
																					printf("%d I, ori %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																					break;
																		}
																	}
																}									    	 	 
					    	 							}                    	          
                    	    }else if (h == 7){
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 
																			
																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {

																							// Inmediato
																							strncpy(interval_imm_I, lineas[i] + 0, 12); 
																							interval_imm_I[12] = '\0';
																							
                                              long decimal = strtol(interval_imm_I, NULL, 2);
                                              
                                              if (interval_imm_I[0] == '1') {
                                                  decimal -= (1L << 12);
                                              }
																							printf("%d I, andi %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																							break;
																		   	}
																			}
																		}									    	 	 
							    		 						}                    	          
                	   					 }
                						}
               					 }                    
                    
                }else if (k == 4){
										// comparar funct3
										strncpy(interval_funct3, lineas[i] + 17, 3); 
										for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
											if (strcmp(interval_funct3, funct3[h]) == 0){
													if ( h == 0){

																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						printf("%d I, adiw %s, %s\n",lin, regist[l].value, regist[t].value);
																						break;
																	}
															}
													}									    	 	 
									 	}
		    	    }else if (h == 1){
		    	   
										// comparar funct7
										strncpy(interval_funct7, lineas[i] + 0, 7); 
										for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
											if (strcmp(interval_funct7, funct7[p]) == 0){
												  if ( p == 0){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0';

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						printf("%d I, slliw %s, %s\n",lin, regist[l].value, regist[t].value);
																						break;
																			}
																		}
																	}									    	 	 
						    	 							}												  	  
									 					  }
														}
												 }
				      
		    	    }else if (h == 5){	    	          
		             // comparar funct7
				       	 strncpy(interval_funct7, lineas[i] + 0, 7); 
				        	for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
				        		if (strcmp(interval_funct7, funct7[p]) == 0){
				        	   	 if ( p == 0){
															//Registro rd						
															strncpy(interval_rd, lineas[i] + 20, 5); 
															interval_rd[5] = '\0'; 

															for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																if (strcmp(interval_rd, regist[l].pos) == 0) {
																
																	// Registro rs1
																	strncpy(interval_rs1, lineas[i] + 12, 5); 
																	interval_rs1[5] = '\0'; 

																	for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																		if (strcmp(interval_rs1, regist[t].pos) == 0) {
																					printf("%d I, srliw %s, %s\n",lin, regist[l].value, regist[t].value);
																					break;
																    }
														      }
												        }									    	 	 
					    	            	}				        	    	 	 
				        	    }else if( p == 1){
															//Registro rd						
															strncpy(interval_rd, lineas[i] + 20, 5); 
															interval_rd[5] = '\0'; 

															for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																if (strcmp(interval_rd, regist[l].pos) == 0) {
																
																	// Registro rs1
																	strncpy(interval_rs1, lineas[i] + 12, 5); 
																	interval_rs1[5] = '\0'; 

																	for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																		if (strcmp(interval_rs1, regist[t].pos) == 0) {
																					printf("%d I, sraiw %s, %s\n",lin, regist[l].value, regist[t].value);
																					break;
														  }
													  }
												  }									    	 	 
										     }				        	    		 
				    	              }
				    	            }
				               }
		                  }
		                }  
	                }
                }else if (k == 10){
										// comparar funct3
										strncpy(interval_funct3, lineas[i] + 17, 3); 
										for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
											if (strcmp(interval_funct3, funct3[h]) == 0){
													if ( h == 0){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 
																
																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 
																		
																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																					  	regist[0].save[0] = 0;
																						  // Inmediato
																							strncpy(interval_imm_I, lineas[i] + 0, 12); 
																							interval_imm_I[12] = '\0';
																							
                       long decimal = strtol(interval_imm_I, NULL, 2);
                       
                       if (interval_imm_I[0] == '1') {
                           decimal -= (1L << 12);
                       }
                       	i = regist[t].save[0] + decimal;
                                              
																								printf("%d I, jalr %s, %s, %ld\n",lin, regist[l].value, regist[t].value, decimal);
																								break;
																}
															}
														}									    	 	 
						    	 					}															                 
									        }
									      }
									    }
                }else if (k == 12){

                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
                    	    	  
																 // comparar IMM
															 	 strncpy(interval_IMM, lineas[i] + 0, 12); 
																for (int z = 0; z < sizeof(IMM) / sizeof(IMM[0]); z++) {
																	if (strcmp(interval_IMM, IMM[z]) == 0){
																	 	 if ( z == 0){
																	 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 
																					
																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									regist[0].save[0] = 0;
																									printf("%d I, ecall %s, %s\n",lin, regist[l].value, regist[t].value);
																									if ( regist[17].save[0] == 32){
																									SDL_Delay(regist[10].save[0]);
 
											}																						
        }else if( z == 1){
																						printf("%d I, ebreak\n",lin);
																						break;	
																						 }													 
																		       }
																		     }
																		   }   																		 	 
																		}
																}
														}
                    	    }else if (h == 1){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 
																					
																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, csrrw %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																				}
																		}
																}									    	 	 
									    	 	}                    	          
                    	    }else if (h == 2){
																		//Registro rd						
																		strncpy(interval_rd, lineas[i] + 20, 5); 
																		interval_rd[5] = '\0'; 

																		for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																			if (strcmp(interval_rd, regist[l].pos) == 0) {
																			
																				// Registro rs1
																				strncpy(interval_rs1, lineas[i] + 12, 5); 
																				interval_rs1[5] = '\0'; 

																				for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																					if (strcmp(interval_rs1, regist[t].pos) == 0) {
                         clock_gettime(CLOCK_REALTIME, &ts);
                         regist[l].save[0] = ts.tv_sec;
																								printf("%d I, csrrs %s = %d , %s \n",lin, regist[l].value,regist[l].save[0], regist[t].value);
																								break;
																			}
																	}
															}									    	 	 
								    	 	}                    	          
                    	    }else if (h == 3){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, csrrc %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																				}
																		}
																}									    	 	 
									    	 	}                    	          
                    	    }else if (h == 5){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																									printf("%d I, csrrwi %s, %s\n",lin, regist[l].value, regist[t].value);
																									break;
																				}
																		}
																}									    	 	 
									    	 	}                    	          
                    	    }else if (h == 6){
																		//Registro rd						
																		strncpy(interval_rd, lineas[i] + 20, 5); 
																		interval_rd[5] = '\0'; 

																		for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																			if (strcmp(interval_rd, regist[l].pos) == 0) {
																			
																				// Registro rs1
																				strncpy(interval_rs1, lineas[i] + 12, 5); 
																				interval_rs1[5] = '\0'; 

																				for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																					if (strcmp(interval_rs1, regist[t].pos) == 0) {
																								printf("%d I, csrrsi %s, %s\n",lin, regist[l].value, regist[t].value);
																								break;
																			}
																	}
															}									    	 	 
								    	 	}                    	          
                    	    }else if (h == 7){
																//Registro rd						
																strncpy(interval_rd, lineas[i] + 20, 5); 
																interval_rd[5] = '\0'; 

																for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																	if (strcmp(interval_rd, regist[l].pos) == 0) {
																	
																		// Registro rs1
																		strncpy(interval_rs1, lineas[i] + 12, 5); 
																		interval_rs1[5] = '\0'; 

																		for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																			if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						printf("%d I, csrrci %s, %s\n",lin, regist[l].value, regist[t].value);
																						break;
																	    }
														      	}
												        	}									    	 	 
						    	 							}                    	          
              	   					 }
              						}
             					 }                                         
              
                }else if (k == 3){

											//Registro rd						
											strncpy(interval_rd, lineas[i] + 20, 5); 
											interval_rd[5] = '\0'; 
											
											for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
												if (strcmp(interval_rd, regist[l].pos) == 0) {
																regist[0].save[0] = 0;
																// Inmediato
																strncpy(interval_imm_U, lineas[i] + 0, 20); 
																interval_imm_U[20] = '\0';
																
                  long decimal = strtol(interval_imm_U, NULL, 2);
                  
                  if (interval_imm_U[0] == '1') {
                      decimal -= (1L << 20);
                  }
                  long pote = (long)pow(2, 12);
		                long potencia = imm_auipc * pote;
                 		int resultado = ((address - 4) + potencia);
                  regist[l].save[0] = resultado;  
                                
																printf("%d U, address:%x auipc %s = %d, %ld\n",lin,address, regist[l].value, regist[l].save[0], decimal);	
																	imm_auipc = decimal;																																														
																break;
												}									    	 	 
    	 							}                    
                }else if (k == 7){

													//Registro rd						
													strncpy(interval_rd, lineas[i] + 20, 5); 
													interval_rd[5] = '\0'; 

													for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
														if (strcmp(interval_rd, regist[l].pos) == 0) {
																	regist[0].save[0] = 0;
																	// Inmediato
																	strncpy(interval_imm_U, lineas[i] + 0, 20); 
																	interval_imm_U[20] = '\0';
																	
                 long decimal = strtol(interval_imm_U, NULL, 2);
                 
                 if (interval_imm_U[0] == '1') {
                     decimal -= (1L << 20);
                 }      
                                  
																	printf("%d U, address:%x lui %s = %d, %ld\n",lin,address, regist[l].value,regist[l].save[0], decimal);
																	imm_lui = decimal;	
																	break;
										}									    	 	 
			    	 	}                   
                } else if (k == 5){
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 
																			
																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 
																					
																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									regist[0].save[0] = 0;
																									// Inmediato
																									strncpy(interval_imm_S1, lineas[i] + 20, 5); 
																									interval_imm_S1[5] = '\0';																									
																									strncpy(interval_imm_S2, lineas[i] + 0, 7); 
																									interval_imm_S2[6] = '\0';
																																																		
																									strcat(interval_imm_S2, interval_imm_S1);																																																
																									long decimal = strtol(interval_imm_S2, NULL, 2);	
																																																
																									if (interval_imm_S2[0] == '1') {
																											decimal -= (1L << 12);
																									}																									
																									printf("%d S, sb %s, %ld(%s)\n",lin, regist[u].value, decimal, regist[t].value);
																									break;
																					 			}
																						}
																				}
																		}
																}									    	 	 
									    	 	}                    	    	  
                    	    }else if (h == 1){
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 
																			
																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {
																				
																					// Registro rs2
																					strncpy(interval_rs2, lineas[i] + 7, 5); 
																					interval_rs2[5] = '\0'; 

																					for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																						if (strcmp(interval_rs2, regist[u].pos) == 0) {
																				   		regist[0].save[0] = 0;
																							// Inmediato
																							strncpy(interval_imm_S1, lineas[i] + 20, 5); 
																							interval_imm_S1[6] = '\0';																									
																							strncpy(interval_imm_S2, lineas[i] + 0, 7); 
																							interval_imm_S2[7] = '\0';
																																																
																							strcat(interval_imm_S2, interval_imm_S1);																																																
																							long decimal = strtol(interval_imm_S2, NULL, 2);	
																																														
																							if (interval_imm_S2[0] == '1') {
																									decimal -= (1L << 12);
																							}																									
																							printf("%d S, sh %s, %ld(%s)\n",lin, regist[u].value, decimal, regist[t].value);
																							break;
																			 			}
																				}
																		}
																}
														}									    	 	 
							    	  	}                   	          
                    	    }else if (h == 2){
																			
																// Registro rs1
																strncpy(interval_rs1, lineas[i] + 12, 5); 
																interval_rs1[5] = '\0'; 

																for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																	if (strcmp(interval_rs1, regist[t].pos) == 0) {
																	
																		// Registro rs2
																		strncpy(interval_rs2, lineas[i] + 7, 5); 
																		interval_rs2[5] = '\0'; 

																		for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																			if (strcmp(interval_rs2, regist[u].pos) == 0) {
																				regist[0].save[0] = 0;
																				int x = 0x10010000;
																				// Inmediato
																				strncpy(interval_imm_S1, lineas[i] + 20, 5); 
																				interval_imm_S1[6] = '\0';																									
																				strncpy(interval_imm_S2, lineas[i] + 0, 7); 
																				interval_imm_S2[7] = '\0';
																																													
																				strcat(interval_imm_S2, interval_imm_S1);																																												
																				long decimal = strtol(interval_imm_S2, NULL, 2);
																																												
																				if (interval_imm_S2[0] == '1') {
																						decimal -= (1L << 12);
																				}
																				if (strcmp(regist[t].value, "sp") == 0) {
                           inst_sp[(sp + decimal) / 4] = regist[u].save[0];
                           printf("%d S, address:%x D.save:%d sw %s, %ld(sp)\n", lin, address, regist[u].save[0], regist[u].value, decimal);
                       }else{
                       
																				long pote = (long)pow(2, 12);
																				long potencia = imm_auipc * pote;
																				
																				int resultado = ((address - 4) + potencia + decimal-x)/4;
																				
																				strncpy(prove, lineas[i-1] + 25, 7); 
																				prove[7] = '\0';
																				
																				if (strcmp(lista[3], prove) == 0) {
																								arreglo[resultado] = regist[u].save[0];
																						
																					  printf("%d S, address:%x Pos_Data:%d D.save:%s sw %s, %ld(%s)\n",lin, address, resultado, lines_Data[resultado], regist[u].value, decimal, regist[t].value);																																				
																				break;
																																													
																				  }else{
																				  printf("%d S, address:%x Pos_Data:%d D.save: sw %s, %ld(%s)\n",lin, address,resultado,regist[u].value, decimal, regist[t].value);	
																				  }
	  																							strncpy(last, lineas[i-1] + 25, 7); 
																										last[7] = '\0';
																						if (strcmp(lista[7], last) == 0 && imm_lui == -16){
																											key = 0 ;
																								
																						}
																 			}
																	}
															}
													}
												}																				    	 	 									    	 	                    	          
                    	    }else if (h == 3){
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 

																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {
																				
																					// Registro rs2
																					strncpy(interval_rs2, lineas[i] + 7, 5); 
																					interval_rs2[5] = '\0'; 

																					for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																						if (strcmp(interval_rs2, regist[u].pos) == 0) {

																									// Inmediato
																									strncpy(interval_imm_S1, lineas[i] + 20, 5); 
																									interval_imm_S1[6] = '\0';																									
																									strncpy(interval_imm_S2, lineas[i] + 0, 7); 
																									interval_imm_S2[7] = '\0';
																																																		
																									strcat(interval_imm_S2, interval_imm_S1);																																												
																									long decimal = strtol(interval_imm_S2, NULL, 2);
																																																	
																									if (interval_imm_S2[0] == '1') {
																											decimal -= (1L << 12);
																									}																									
																									printf("%d S, sd, %s, %ld(%s)\n",lin, regist[u].value, decimal, regist[t].value);
																									break;
																			 			}
																				}
																		}
																}
														}									    	 	 
							    	   	}                    	          
                	   }
                	}
               }
                } else if (k == 6){
                	
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
                    	    	  
														 // comparar funct7
													 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
														for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
															if (strcmp(interval_funct7, funct7[p]) == 0){
															 	 if ( p == 0){
																	 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																		      // Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 																						
																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									   regist[0].save[0] = 0;																							
																								    regist[l].save[0] = regist[t].save[0] + regist[u].save[0];																							    
																								    	strncpy(last, lineas[i+1] + 25, 7); 
        																					last[7] = '\0';
																										if(strcmp(regist[u].value, "gp") == 0 && strcmp(regist[l].value, "t1") == 0 && strcmp(last, lista[5]) == 0){																																																																									
																															if ((regist[11].save[0] *64 + regist[10].save[0]) <= 0 || (regist[11].save[0] *64 + regist[10].save[0]) > 8000){								
																																				regist[10].save[0] = 0;
																																				regist[11].save[0] = 0;
																															}
																															int coord = (regist[11].save[0] *64 + regist[10].save[0]);																																																										
																															pixels[coord].pos_x =  regist[10].save[0];
																															pixels[coord].pos_y =  regist[11].save[0];
																															pixels[coord].color =  regist[12].save[0];																																																														
																															Draw(renderer);
																															SDL_RenderPresent(renderer);																																																																									
																											}if(strcmp(regist[u].value, "gp") == 0 && strcmp(regist[l].value, "t3") == 0){																																				
																																		for (int coord = 0; coord <= num_pixels; coord++){
																																							pixels[coord].color = 0x00000000;
																																							regist[7].save[0] = 8188;
																																		}																																																																						
																																		Draw(renderer);																																																												
																											}																							
																									  printf("%d R, address:%x add %s = %d, %s = %d, %s = %d \n",lin, address, regist[l].value,regist[l].save[0], regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0]);
																						 			break;
																					 				}
																								}
																						}
																				}
																		}
																}
																}else if( p == 1){																 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																										regist[0].save[0] = 0;	
																								    regist[l].save[0] = regist[t].save[0] - regist[u].save[0];
																				
																									printf("%d R, address:%x sub %s = %d, %s = %d, %s = %d \n",lin, address, regist[l].value,regist[l].save[0], regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0]);
																						 			break;
																				 				}
																							}
																					}
																			}
																	}
															}																		 
													}
						  				}
						       }
                    	    }else if (h == 1){
                    	          
																// comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																										regist[0].save[0] = 0;												
																								    regist[l].save[0] = regist[t].save [0] * pow(2,regist[u].save[0]);
																								
																									  printf("%d R,address:%x sll %s = %d, %s = %d, %s = %d\n",lin,address, regist[l].value,regist[l].save[0], regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0]);
																						 			break;
																											 			}
																												}
																										}
																								}
																						}
																				}																		 	 
																		}
																}
														}
                    	    }else if (h == 2){
                    	          
															 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, slt %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																											 			}
																												}
																										}
																								}
																						}
																				}																		 	 
																	  }
															  }
														 }
                    	    }else if (h == 3){                  	          
																// comparar funct7
															 	strncpy(interval_funct7, lineas[i] + 0, 7); 
																for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																	if (strcmp(interval_funct7, funct7[p]) == 0){
																	 	 if ( p == 0){
																			 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, sltu %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																							}
																						}
																					}
																				}
																			}																			 	 
																		}
																	}
								 								}
                    	    }else if (h == 4){                    	          
															 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																										  regist[0].save[0] = 0;																												
																								    regist[l].save[0] = regist[t].save[0] ^ regist[u].save[0];
																								
																									  printf("%d R, xor %s = %d, %s = %d, %s = %d\n",lin, regist[l].value,regist[l].save[0], regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0]);
																						 			break;
																					 			}
																							}
																						}
																					}
																				}
																			}																		 	 
																	 }
									 							}
															}
                    	    }else if (h == 5){											        	          
															 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 
																			
																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, srl %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																							}
																						}
																					}
																				}
																			}																		 	 
																		 	 
																	}else if( p == 1){
																			 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, sra %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																						  }
																						}
																					}
																				}
																			}																			 
											 						 }
									   						}
								  					}                    	         
					        	    }else if (h == 6){      	         	 
													// comparar funct7
												 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
													for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
														if (strcmp(interval_funct7, funct7[p]) == 0){
														 	 if ( p == 0){
																 	 
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 

																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {
																				
																					// Registro rs2
																					strncpy(interval_rs2, lineas[i] + 7, 5); 
																					interval_rs2[5] = '\0'; 

																					for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																						if (strcmp(interval_rs2, regist[u].pos) == 0) {
																							printf("%d R, or %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																				 			break;
																			 			}
																				  }
																		   }
																 		}
												    		 }																							 	 
									      			}
							      				}
						    					}
						 						}
									    }else if (h == 7){									    	          
													// comparar funct7
												 	strncpy(interval_funct7, lineas[i] + 0, 7); 
													for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
														if (strcmp(interval_funct7, funct7[p]) == 0){
														 	 if ( p == 2){
																 	 
																	//Registro rd						
																	strncpy(interval_rd, lineas[i] + 20, 5); 
																	interval_rd[5] = '\0'; 

																	for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																		if (strcmp(interval_rd, regist[l].pos) == 0) {
																		
																			// Registro rs1
																			strncpy(interval_rs1, lineas[i] + 12, 5); 
																			interval_rs1[5] = '\0'; 

																			for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																				if (strcmp(interval_rs1, regist[t].pos) == 0) {
																				
																					// Registro rs2
																					strncpy(interval_rs2, lineas[i] + 7, 5); 
																					interval_rs2[5] = '\0'; 

																					for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																						if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									regist[l].save[0] = regist[t].save[0] % regist[u].save[0];
																							printf("%d R, remu %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																				 			break;
																			 			}
																					}
																				}
																			}
																		}																					 	 																		 	 
																	}
																}
														 }
						 							}
												}
											}		
										}
							
                }else if (k == 8){
                    
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
                    	    	  
															 // comparar funct7
													 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
														for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
															if (strcmp(interval_funct7, funct7[p]) == 0){
															 	 if ( p == 0){
																		//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, addw %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																						}
																				}
																		}
																}									    	 	 
									    	 	} 
									    					}else if( p == 1){
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, subw %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																							}
																						}
																					}
																				}									    	 	 
									    	 							}									    						 
									   							 }
																}
															}                    	    	  
                    	    }else if (h == 1){                 	          
															 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0'; 

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, addw %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																							}
																						}
																					}
																				}									    	 	 
									    	  					 }																		 	 
																	 }
																}
															}                    	          
                    	    }else if (h == 5){                   	          
																 // comparar funct7
														 	 strncpy(interval_funct7, lineas[i] + 0, 7); 
															for (int p = 0; p < sizeof(funct7) / sizeof(funct7[0]); p++) {
																if (strcmp(interval_funct7, funct7[p]) == 0){
																 	 if ( p == 0){
																		 	 
																			//Registro rd						
																			strncpy(interval_rd, lineas[i] + 20, 5); 
																			interval_rd[5] = '\0'; 

																			for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
																				if (strcmp(interval_rd, regist[l].pos) == 0) {
																				
																					// Registro rs1
																					strncpy(interval_rs1, lineas[i] + 12, 5); 
																					interval_rs1[5] = '\0';

																					for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																						if (strcmp(interval_rs1, regist[t].pos) == 0) {
																						
																							// Registro rs2
																							strncpy(interval_rs2, lineas[i] + 7, 5); 
																							interval_rs2[5] = '\0'; 

																							for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																								if (strcmp(interval_rs2, regist[u].pos) == 0) {
																									printf("%d R, srlw %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																						 			break;
																					 			}
																						}
																				}
																		}
																}									    	 	 
									    	 	}									    	 	 
									    }else if( p == 1){
														//Registro rd						
														strncpy(interval_rd, lineas[i] + 20, 5); 
														interval_rd[5] = '\0'; 

														for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
															if (strcmp(interval_rd, regist[l].pos) == 0) {
															
																// Registro rs1
																strncpy(interval_rs1, lineas[i] + 12, 5); 
																interval_rs1[5] = '\0'; 
																
																for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																	if (strcmp(interval_rs1, regist[t].pos) == 0) {
																	
																		// Registro rs2
																		strncpy(interval_rs2, lineas[i] + 7, 5); 
																		interval_rs2[5] = '\0'; 

																		for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																			if (strcmp(interval_rs2, regist[u].pos) == 0) {
																				printf("%d R, sraw %s, %s, %s\n",lin, regist[l].value, regist[t].value, regist[u].value);
																	 			break;
																 			}
																		}
																	}
																}
															}									    	 	 
				    	 							}									    		 
				    							}
				  							}
			 								}                    	          
     								}
   								}
 								}                   
                    
                } else if (k == 9){
                    // comparar funct3
                    strncpy(interval_funct3, lineas[i] + 17, 3); 
                    for (int h = 0; h < sizeof(funct3) / sizeof(funct3[0]); h++) {
                    	if (strcmp(interval_funct3, funct3[h]) == 0){
                    	    if ( h == 0){
															// Registro rs1
															strncpy(interval_rs1, lineas[i] + 12, 5); 
															interval_rs1[5] = '\0'; 

															for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																if (strcmp(interval_rs1, regist[t].pos) == 0) {
																
																	// Registro rs2
																	strncpy(interval_rs2, lineas[i] + 7, 5); 
																	interval_rs2[5] = '\0'; 

																	for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																		if (strcmp(interval_rs2, regist[u].pos) == 0) {

																				regist[0].save[0] = 0;
																				strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																				interval_Imm_SB1[7] = '\0';
																				strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																				interval_Imm_SB2[5] = '\0';

																				strcat(interval_Imm_SB1, interval_Imm_SB2);
																				
																				char bit_12 = interval_Imm_SB1[0];
																				char bit_11 = interval_Imm_SB1[11];

																				strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																				interval_imm_Sb1[6] = '\0';

																				strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																				interval_imm_Sb2[4] = '\0';

																				sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																				interval_Immediate[12] = '\0';
																				strcat(interval_Immediate, "0");

																				long decimal = strtol(interval_Immediate, NULL, 2);
																				if (interval_Immediate[0] == '1') {
																							decimal -= (1L << 13);
																					}
																					
																						if (regist[t].save[0] == regist[u].save[0]){
																							i +=  decimal/4 - 1;                             
																						}										
                       printf("%d SB, address:%x beq %s = %d, %s = %d, %ld\n", lin, address, regist[t].value,regist[t].save[0], regist[u].value, regist[u].save[0], decimal);
                       break;
																 		}
																 	}
																}
															}	 		                  	    	  
                    	 }else if (h == 1){
															// Registro rs1
															strncpy(interval_rs1, lineas[i] + 12, 5); 
															interval_rs1[5] = '\0'; 

															for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																if (strcmp(interval_rs1, regist[t].pos) == 0) {
																
																	// Registro rs2
																	strncpy(interval_rs2, lineas[i] + 7, 5); 
																	interval_rs2[5] = '\0'; 

																	for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																		if (strcmp(interval_rs2, regist[u].pos) == 0) {

																		regist[0].save[0] = 0;
																		strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																		interval_Imm_SB1[7] = '\0';
																		strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																		interval_Imm_SB2[5] = '\0';

																		strcat(interval_Imm_SB1, interval_Imm_SB2);
																		
																		char bit_12 = interval_Imm_SB1[0];
																		char bit_11 = interval_Imm_SB1[11];

																		strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																		interval_imm_Sb1[6] = '\0';

																		strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																		interval_imm_Sb2[4] = '\0';

																		sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																		interval_Immediate[12] = '\0';
																		strcat(interval_Immediate, "0");

																		long decimal = strtol(interval_Immediate, NULL, 2);
																		if (interval_Immediate[0] == '1') {
																					decimal -= (1L << 13);
																			}
																				if (regist[t].save[0] != regist[u].save[0]){
																					i +=decimal/4 - 1;
																					
																				}
						               printf("%d SB, address:%x bne %s, %s, %ld\n", lin, address, regist[t].value, regist[u].value, decimal);
						               break;
																 		}
																 	}
																}
															}                   	          
                    	    }else if (h == 4){
																				// Registro rs1
																				strncpy(interval_rs1, lineas[i] + 12, 5); 
																				interval_rs1[5] = '\0'; 

																				for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																					if (strcmp(interval_rs1, regist[t].pos) == 0) {
																					
																						// Registro rs2
																						strncpy(interval_rs2, lineas[i] + 7, 5); 
																						interval_rs2[5] = '\0'; 

																						for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																							if (strcmp(interval_rs2, regist[u].pos) == 0) {
																							regist[0].save[0] = 0;
																							
																							strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																							interval_Imm_SB1[7] = '\0';
																							strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																							interval_Imm_SB2[5] = '\0';

																							strcat(interval_Imm_SB1, interval_Imm_SB2);
																							
																							char bit_12 = interval_Imm_SB1[0];
																							char bit_11 = interval_Imm_SB1[11];

																							strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																							interval_imm_Sb1[6] = '\0';

																							strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																							interval_imm_Sb2[4] = '\0';

																							sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																							interval_Immediate[12] = '\0';
																							strcat(interval_Immediate, "0");

																							long decimal = strtol(interval_Immediate, NULL, 2);
																							if (interval_Immediate[0] == '1') {
																										decimal -= (1L << 13);
																								}
																									if (regist[t].save[0] < regist[u].save[0]){
																										i += decimal/4 - 1;
																										
																									}
                    printf("%d SB, blt %s = %d, %s = %d, %ld\n", lin, regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0], decimal);
                    break;
																 		}
																 	}
																}
															}                    	          
                    	    }else if (h == 5){
															// Registro rs1
															strncpy(interval_rs1, lineas[i] + 12, 5); 
															interval_rs1[5] = '\0'; 

															for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																if (strcmp(interval_rs1, regist[t].pos) == 0) {
																
																	// Registro rs2
																	strncpy(interval_rs2, lineas[i] + 7, 5); 
																	interval_rs2[5] = '\0'; 

																	for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																		if (strcmp(interval_rs2, regist[u].pos) == 0) {
																		
																		regist[0].save[0] = 0;
																		
																		strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																		interval_Imm_SB1[7] = '\0';
																		strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																		interval_Imm_SB2[5] = '\0';

																		strcat(interval_Imm_SB1, interval_Imm_SB2);
																		
																		char bit_12 = interval_Imm_SB1[0];
																		char bit_11 = interval_Imm_SB1[11];

																		strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																		interval_imm_Sb1[6] = '\0';

																		strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																		interval_imm_Sb2[4] = '\0';

																		sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																		interval_Immediate[12] = '\0';
																		strcat(interval_Immediate, "0");

																		long decimal = strtol(interval_Immediate, NULL, 2);
																		if (interval_Immediate[0] == '1') {
																					decimal -= (1L << 13);
																			}
																				if (regist[t].save[0] >= regist[u].save[0]){
																					i += decimal/4 - 1;
																					
																				}
                                        printf("%d SB, bge %s = %d, %s= %d, %ld\n", lin, regist[t].value,regist[t].save[0], regist[u].value,regist[u].save[0], decimal);
                                        break;
																 		}
																 	}
																}
															}                    	          
                    	    }else if (h == 6){
															// Registro rs1
															strncpy(interval_rs1, lineas[i] + 12, 5); 
															interval_rs1[5] = '\0'; 

															for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																if (strcmp(interval_rs1, regist[t].pos) == 0) {
																
																	// Registro rs2
																	strncpy(interval_rs2, lineas[i] + 7, 5); 
																	interval_rs2[5] = '\0'; 

																	for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																		if (strcmp(interval_rs2, regist[u].pos) == 0) {

																		strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																		interval_Imm_SB1[7] = '\0';
																		strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																		interval_Imm_SB2[5] = '\0';

																		strcat(interval_Imm_SB1, interval_Imm_SB2);
																		
																		char bit_12 = interval_Imm_SB1[0];
																		char bit_11 = interval_Imm_SB1[11];

																		strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																		interval_imm_Sb1[6] = '\0';

																		strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																		interval_imm_Sb2[4] = '\0';

																		sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																		interval_Immediate[12] = '\0';
																		strcat(interval_Immediate, "0");

																		long decimal = strtol(interval_Immediate, NULL, 2);
																		if (interval_Immediate[0] == '1') {
																					decimal -= (1L << 13);
																			}

                     printf("%d SB, bltu %s, %s, %ld\n", lin, regist[t].value, regist[u].value, decimal);
                     break;
																 		}
																 	}
																}
															}                    	          
                    	    }else if (h == 7){
															// Registro rs1
															strncpy(interval_rs1, lineas[i] + 12, 5); 
															interval_rs1[5] = '\0'; 

															for (int t = 0; t < sizeof(regist) / sizeof(regist[0]); t++) {
																if (strcmp(interval_rs1, regist[t].pos) == 0) {
																
																	// Registro rs2
																	strncpy(interval_rs2, lineas[i] + 7, 5); 
																	interval_rs2[5] = '\0'; 

																	for (int u = 0; u < sizeof(regist) / sizeof(regist[0]); u++) {
																		if (strcmp(interval_rs2, regist[u].pos) == 0) {

																		strncpy(interval_Imm_SB1, lineas[i] + 0, 7);
																		interval_Imm_SB1[7] = '\0';
																		strncpy(interval_Imm_SB2, lineas[i] + 20, 5);
																		interval_Imm_SB2[5] = '\0';

																		strcat(interval_Imm_SB1, interval_Imm_SB2);
																		
																		char bit_12 = interval_Imm_SB1[0];
																		char bit_11 = interval_Imm_SB1[11];

																		strncpy(interval_imm_Sb1, interval_Imm_SB1 + 1, 6);
																		interval_imm_Sb1[6] = '\0';

																		strncpy(interval_imm_Sb2, interval_Imm_SB1 + 7, 4);
																		interval_imm_Sb2[4] = '\0';
																		
																		sprintf(interval_Immediate, "%c%c%s%s", bit_12, bit_11, interval_imm_Sb1, interval_imm_Sb2);
																		interval_Immediate[12] = '\0';
																		strcat(interval_Immediate, "0");
																		long decimal = strtol(interval_Immediate, NULL, 2);
																		if (interval_Immediate[0] == '1') {
																					decimal -= (1L << 13);
																			}

                     printf("%d SB, bgeu %s, %s, %ld\n", lin, regist[t].value, regist[u].value, decimal);
                     break;
																 		}
																 	}
																}
															}                    	          
                    	    }
                    	}
                    }                    
                }else if (k == 11){

										//Registro rd						
										strncpy(interval_rd, lineas[i] + 20, 5); 
										interval_rd[5] = '\0'; 

										for (int l = 0; l < sizeof(regist) / sizeof(regist[0]); l++) {
											if (strcmp(interval_rd, regist[l].pos) == 0) {
											
												regist[0].save[0] = 0;											
	 											strncpy(interval_Imm_UJ, lineas[i] + 0, 20);
												interval_Imm_UJ[20] = '\0';
												
												strncpy(interval_Imm_UJ1, interval_Imm_UJ + 1, 10);
												interval_Imm_UJ1[10] = '\0';
												strncpy(interval_Imm_UJ2, interval_Imm_UJ + 12, 8);
												interval_Imm_UJ2[8] = '\0';
												

												char bit_20 = interval_Imm_UJ[0];
												char bit_11U = interval_Imm_UJ[11];
												sprintf(interval_Immediat, "%c%s%c%s", bit_20, interval_Imm_UJ2, bit_11U, interval_Imm_UJ1);
 
												interval_Immediat[20] = '\0';
												strcat(interval_Immediat, "0");
												
												long decimal = strtol(interval_Immediat, NULL, 2);
												if (interval_Immediat[0] == '1') {
															decimal -= (1L << 21);
													}
													regist[l].save[0] = i;
													i += decimal/4-1;
													
											    printf("%d UJ, address:%x jal %s, %ld\n",lin, address, regist[l].value, decimal);											 												 
											}
										}	                  
                }
            }
        }i++;
    } 
    // Limpiar recursos y cerrar SDL
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();   
    /*
    for (int i = 0; i < num_lineas; i++)
        free(lineas[i]);
    free(lineas);
    
    // Liberar la memoria asignada
    for (int y = 0; y < num_lines_data; y++) {
        free(lines_Data[y]);
    }
    free(lines_Data);	
		*/
    return 0;
}
//REFERENCIAS

//Green Card: https://inst.eecs.berkeley.edu/~cs61c/fa17/img/riscvcard.pdf
//Funtions registers: https://www.cs.sfu.ca/~ashriram/Courses/CS295/assets/notebooks/RISCV/RISCV_CARD.pdf
//Reserve variables: https://www.w3schools.com/c/c_strings_functions.php
//sprintf: https://www.puntoflotante.net/strings.htm






